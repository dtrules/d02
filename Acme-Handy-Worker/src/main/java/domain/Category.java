
package domain;

import java.util.Collection;

import org.hibernate.validator.constraints.NotBlank;

public class Category extends DomainEntity {

	// Attibutes

	private String					name;

	private Category				parentCategory;
	private Collection<Category>	subCategories;


	// Getters & setters

	@NotBlank
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	// Relationships ----------------------------------------------------------

	public Category getParentCategory() {
		return this.parentCategory;
	}

	public void setParentCategory(final Category parentCategory) {
		this.parentCategory = parentCategory;
	}

	public Collection<Category> getSubCategories() {
		return this.subCategories;
	}

	public void setSubCategories(final Collection<Category> subCategories) {
		this.subCategories = subCategories;
	}

}
