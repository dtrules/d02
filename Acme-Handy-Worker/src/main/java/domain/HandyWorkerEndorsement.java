
package domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class HandyWorkerEndorsement extends Endorsement {

	// Attributes

	private Customer	customer;


	/* private double score //(derivada) */

	// Getters & setters

	@Valid
	@NotNull
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(final Customer customer) {
		this.customer = customer;
	}
}
