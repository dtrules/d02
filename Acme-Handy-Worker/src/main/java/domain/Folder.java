
package domain;

import java.util.Collection;

import org.hibernate.validator.constraints.NotBlank;

public class Folder extends DomainEntity {

	// Attributes

	private String				name;

	private Collection<Message>	messages;


	// Getters & setters

	@NotBlank
	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	// Relationships ----------------------------------------------------------

	public Collection<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(final Collection<Message> messages) {
		this.messages = messages;
	}

}
