
package domain;

import java.util.Collection;

public class Referee extends Actor {

	// Attributes

	private Collection<Report>	reports;


	// Getters & setters

	// Relationships ----------------------------------------------------------

	public Collection<Report> getReports() {
		return this.reports;
	}

	public void setReports(final Collection<Report> reports) {
		this.reports = reports;
	}
}
