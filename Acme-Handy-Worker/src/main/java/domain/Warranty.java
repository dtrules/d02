
package domain;

import org.hibernate.validator.constraints.NotBlank;

public class Warranty extends DomainEntity {

	// Attributes

	private String	title;
	private String	terms;
	private String	laws;


	// Getters & setters

	@NotBlank
	public String getTitle() {
		return this.title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	@NotBlank
	public String getTerms() {
		return this.terms;
	}

	public void setTerms(final String terms) {
		this.terms = terms;
	}

	@NotBlank
	public String getLaws() {
		return this.laws;
	}

	public void setLaws(final String laws) {
		this.laws = laws;
	}

	// Relationships ----------------------------------------------------------

}
