
package domain;

import java.util.Collection;

public class Customer extends Actor {

	// Attributes

	private Collection<CustomerEndorsement>	customerEndorsements;
	private Collection<Task>				tasks;


	// Getters & setters

	public Collection<CustomerEndorsement> getCustomerEndorsements() {
		return this.customerEndorsements;
	}

	public void setCustomerEndorsements(final Collection<CustomerEndorsement> customerEndorsements) {
		this.customerEndorsements = customerEndorsements;
	}

	public Collection<Task> getTasks() {
		return this.tasks;
	}

	public void setTasks(final Collection<Task> tasks) {
		this.tasks = tasks;
	}
}
