
package utilities;

public class FootballPlayer {

	public String	id;
	public String	name;
	public String	surname;
	public String	position;
	public String	dorsal;
	public String	team;


	@Override
	public String toString() {
		return this.id + "," + this.name + ", " + this.surname + ", " + this.position + ", " + this.dorsal + ", " + this.team;
	}
}
