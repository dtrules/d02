
package domain;

import java.util.Collection;

import javax.validation.constraints.NotNull;

public class Sponsor extends Actor {

	// Attributes

	private Collection<Sponsorship>	sponsorships;


	// Getters & setters

	// Relationships ----------------------------------------------------------

	@NotNull
	public Collection<Sponsorship> getSponsorships() {
		return this.sponsorships;
	}

	public void setSponsorships(final Collection<Sponsorship> sponsorships) {
		this.sponsorships = sponsorships;
	}

}
