
package domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CustomerEndorsement extends Endorsement {

	// Attributes

	private HandyWorker	handyWorker;


	/* private double score //(derivada) */

	// Getters & setters

	@Valid
	@NotNull
	public HandyWorker getHandyWorker() {
		return this.handyWorker;
	}

	public void setHandyWorker(final HandyWorker handyWorker) {
		this.handyWorker = handyWorker;
	}
}
