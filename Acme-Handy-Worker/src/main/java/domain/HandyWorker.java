
package domain;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class HandyWorker extends Actor {

	// Attributes

	private String								make;

	private Collection<Application>				applications;
	private Curriculum							curriculum;
	private Finder								finder;
	private Collection<HandyWorkerEndorsement>	handyWorkerEndorsements;


	// Getters & setters

	@NotBlank
	public String getMake() {
		return this.make;
	}

	public void setMake(final String make) {
		this.make = make;
	}

	// Relationships ----------------------------------------------------------

	public Collection<Application> getApplications() {
		return this.applications;
	}

	public void setApplications(final Collection<Application> applications) {
		this.applications = applications;
	}

	@NotNull
	public Curriculum getCurriculum() {
		return this.curriculum;
	}

	public void setCurriculum(final Curriculum curriculum) {
		this.curriculum = curriculum;
	}

	@NotNull
	public Finder getFinder() {
		return this.finder;
	}

	public void setFinder(final Finder finder) {
		this.finder = finder;
	}

	public Collection<HandyWorkerEndorsement> getHandyWorkerEndorsements() {
		return this.handyWorkerEndorsements;
	}

	public void setEndorsements(final Collection<HandyWorkerEndorsement> handyWorkerEndorsements) {
		this.handyWorkerEndorsements = handyWorkerEndorsements;
	}

}
