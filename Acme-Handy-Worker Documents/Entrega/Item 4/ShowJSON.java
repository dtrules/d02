
package utilities;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

public class ShowJSON {

	/**
	 * 
	 * @param args
	 * @throws FileNotFoundException
	 */

	public static void main(final String[] args) throws FileNotFoundException {

		final Type collectionType = new TypeToken<Collection<FootballPlayer>>() {
		}.getType();

		final Gson gson = new Gson();

		final JsonReader reader = new JsonReader(new FileReader("src/main/java/utilities/FootballPlayers.json"));

		final Collection<FootballPlayer> data = gson.fromJson(reader, collectionType);

		for (final FootballPlayer f : data)

			System.out.println(f.toString());

	}

}
